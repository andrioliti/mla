from flask import Flask, render_template

from flask_sqlalchemy import SQLAlchemy
from flask_restful import Resource, Api

# Define the WSGI application object
app = Flask(__name__)
api = Api(app)

# Configurations
app.config.from_object('config')

# Define the database object which is imported
# by modules and controllers
db = SQLAlchemy(app)

# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

#REST
from app.auth.resource import TodoResource
api.add_resource(TodoResource, '/todo')

# Build the database:
# This will create the database file using SQLAlchemy
# db.create_all()