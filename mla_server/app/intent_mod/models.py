from app.base_mod.models import Base
from app import db

class Intent(Base):
    
    __tablename__ = 'intent'

    name = db.Column(db.String(50), nullable=False)
    corpus_id = db.Column(db.Integer, db.ForeignKey('corpus.id'), nullable=False)
    corpus = db.relationship('Corpus', backref=db.backref('intents', lazy=True))

class IntentValue(Base):
    
    __tablename__ = 'intent_value'
    
    value = db.Column(db.String(50), nullable=False)
    intent_id = db.Column(db.Integer, db.ForeignKey('intent.id'), nullable=False)
    intent = db.relationship('Intent', backref=db.backref('intents', lazy=True))