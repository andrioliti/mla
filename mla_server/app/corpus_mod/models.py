from app.base_mod.models import Base
from app import db

class Corpus(Base):
    
    __tablename__ = 'corpus'

    name = db.Column(db.String(50), nullable=False)
    description = db.Column(db.Text)

