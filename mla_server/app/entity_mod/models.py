from app.base_mod.models import Base
from app import db

class Entity(Base):
    
    __tablename__ = 'entity'

    name = db.Column(db.String(50), nullable=False)

    corpus_id = db.Column(db.Integer, db.ForeignKey('corpus.id'), nullable=False)
    corpus = db.relationship('Corpus', backref=db.backref('entities', lazy=True))

class EntityValue(Base):
    __tablename__ = 'entity_value'
    
    value = db.Column(db.String(50), nullable=False)
    entity_id = db.Column(db.Integer, db.ForeignKey('entity.id'), nullable=False)
    entity = db.relationship('Entity', backref=db.backref('entities', lazy=True))