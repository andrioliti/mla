drop table if exists "corpus";
drop table if exists "entity";
drop table if exists "intent";
drop table if exists "value_entity";
drop table if exists "value_intent";


create table "corpus"(
    id serial primary key,
    name varchar(50),
    description text
);

create table "entity"(
    id serial primary key,
    name varchar(50)
);

create table "intent"(
    id serial primary key,
    name varchar(50)
);

create table "value_entity"(
    id serial primary key,
    value text
);

create table "value_intent"(
    id serial primary key,
    value text
);

alter table value_entity add column entity_id integer;

alter table value_intent add column intent_id integer;

alter table value_entity 
    add constraint fk_entity 
    foreign key(entity_id) 
    REFERENCES entity(id);


alter table value_intent
    add constraint fk_intent 
    foreign key(intent_id) 
    REFERENCES intent(id);


alter table entity add column corpus_id integer;

alter table intent add column corpus_id integer;

alter table entity 
    add constraint fk_corpus_entity
    foreign key(corpus_id) 
    REFERENCES corpus(id);


alter table intent
    add constraint fk_corpus_intent 
    foreign key(corpus_id) 
    REFERENCES corpus(id);

-- Auto-generated SQL script #202011201539
INSERT INTO public.corpus (id,"name",description)
	VALUES (1,'corpus_test','Oi, eu sou um corpus, mas porderia ser uma galáxia todinha');

-- Auto-generated SQL script #202011201542
INSERT INTO public.entity (id,"name",corpus_id)
	VALUES (1,'confirmar',1);

INSERT INTO public.entity (id,"name",corpus_id)
	VALUES (2,'negar',1);

-- Auto-generated SQL script #202011201544
INSERT INTO public.intent (id,"name",corpus_id)
	VALUES (1,'consultar',1);
INSERT INTO public.intent (id,"name",corpus_id)
	VALUES (2,'reclamar',1);


-- Auto-generated SQL script #202011201545
INSERT INTO public.value_entity (id,value,entity_id)
	VALUES (1,'sim',1);
INSERT INTO public.value_entity (id,value,entity_id)
	VALUES (2,'aceito',1);
INSERT INTO public.value_entity (id,value,entity_id)
	VALUES (3,'não quero',2);

-- Auto-generated SQL script #202011201548
INSERT INTO public.value_intent (id,value,intent_id)
	VALUES (1,'meu saldo',1);
INSERT INTO public.value_intent (id,value,intent_id)
	VALUES (2,'horrível',2);
INSERT INTO public.value_intent (id,value,intent_id)
	VALUES (3,'limite do cartão',1);
